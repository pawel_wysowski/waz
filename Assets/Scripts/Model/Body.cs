﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using System;

[Serializable]
public class Body{

	public Sprite Image;
	public  Vector3 Position;

}

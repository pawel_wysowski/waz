﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class GameModel : Model<SnakeApplication> {

	public const string GENERATED_FOOD = "GameModel.GeneratedFood";
	public const string START_POS = "GameModel.StartBodyPosition";
	public const string TIME_TO_MOVE = "GameModel.TimeToMove";
	public const string MOVE_UP = "GameModel.MoveUp";
	public const string MOVE_DOWN = "GameModel.MoveDown";
	public const string MOVE_LEFT = "GameModel.MoveLeft";
	public const string MOVE_RIGHT = "GameModel.MoveRight";
	public const string GAME_OVER = "GameModel.GameOver";
	public const string EAT_APPLE = "GameModel.EatApple";
	public const string EAT_APPLE_EXTRA = "GameModel.EatAppleExtra";
	public const string BACK_TO_MAIN_MENU = "GameModel.BackToMainMenu";
	public const string GENERATED_EXTRA_FOOD = "GameModel.ExtraFood";
	public const string MOBILE_PLATFORM	= "GameModel.MobilePlatform";

	public static int width = 70;
	public static int height = 90;
	public float timeLeft = 0.1f;
	public char [,] Board = new char[width,height];
	public Direction currentDirection;
	public Vector3 currentHeadPos;
	public Vector3 previousHeadPos;
	public int SnakeLength = 1;
	public Turn currentGameState = Turn.inMenu;

	public enum Turn
	{
		inGame,
		inMenu,
	}

	public enum Direction
	{
		up,
		down,
		left,
		right,
	
	}

	public void ChangeGameState(){
		switch (currentGameState) {
		case Turn.inMenu:
			currentGameState = Turn.inGame;
			break;
		case Turn.inGame:
			currentGameState = Turn.inMenu;
			break;
		}
	}

	public void RandomExtraFoodPosition(){
		int x = Random.Range (20, 60);
		int y = Random.Range (20, 80);
		Board [x, y] = 's';
		Vector3 position = new Vector3 (x, y, 1);
		Notify (GENERATED_EXTRA_FOOD, position);
	}

	public void RandomFoodPosition(){
		int x = Random.Range (20, 60);
		int y = Random.Range (20, 80);

		Board [x, y] = 'o';
		Vector3 position = new Vector3 (x, y, 1);
		Notify (GENERATED_FOOD, position);
	}

	public void ResetBoard(){
		for(int i = 0; i<width; i++){
			for(int j = 0; j<height ; j++){
				Board [i, j] = '-';
			}	
		}
	}

	public void ResetTime(){
		timeLeft = 0.2f;
	}

	public Vector3 GetCurrentVectorPos(){
		return currentHeadPos;
	}

	public void StartBodyPosition(){
		Vector3 vector = new Vector3 (35, 50, 1);
		Board [35, 50] = 'x';
		currentHeadPos = vector;
		RandomFoodPosition ();
		//
		RandomExtraFoodPosition ();
		Notify (START_POS, vector);
	}

	public void MoveSnakeBody(Vector3 vector){
		int x = (int)vector.x;
		int y = (int)vector.y;

		if (currentDirection == Direction.right) {
			int xNew = x+1;
			if (xNew > 69)
				Notify (GAME_OVER);
			if (Board [xNew, y] == 'x') {
				Debug.Log ("CrashR");
				Notify (GAME_OVER);
			}

			if (Board [xNew, y] == 'o') {
				Debug.Log ("APPLE");
//				SnakeLength++;
				Vector3 applePos = new Vector3 (xNew, y, 1);
				Notify (EAT_APPLE, applePos);
//				Board [xNew, y] = 'x';
//				Board [x, y] = 'x';
			}

			if (Board [xNew, y] == 's') {
				Debug.Log ("APPLE_EXTRA");
				//				SnakeLength++;
				Vector3 applePos = new Vector3 (xNew, y, 1);
				Notify (EAT_APPLE_EXTRA, applePos);
				//				Board [xNew, y] = 'x';
				//				Board [x, y] = 'x';
			}


			Board [xNew, y] = 'x';
			Vector3 vectorR = new Vector3 (xNew, y, 1);
			currentHeadPos = vectorR;
			Notify (MOVE_RIGHT, vectorR);
		}
		if (currentDirection == Direction.left) {
			int xNew = x-1;
			if (xNew < 0)
				Notify (GAME_OVER);
//			if (Board [xNew, y] == 'x') {
//				Debug.Log ("CrashL");
//				Notify (GAME_OVER);
//			}
			if (Board [xNew, y] == 'o') {
				Debug.Log ("APPLE");
				SnakeLength++;
				Vector3 applePos = new Vector3 (xNew, y, 1);

				Notify (EAT_APPLE, applePos);
//				Board [xNew, y] = 'x';
//				Board [x, y] = 'x';
			}

			if (Board [xNew, y] == 's') {
				Debug.Log ("APPLE_EXTRA");
				//				SnakeLength++;
				Vector3 applePos = new Vector3 (xNew, y, 1);
				Notify (EAT_APPLE_EXTRA, applePos);
				//				Board [xNew, y] = 'x';
				//				Board [x, y] = 'x';
			}

			//
			Vector3 vectorL = new Vector3 (xNew, y, 1);
			currentHeadPos = vectorL;
			Board [xNew, y] = 'x';
			Notify (MOVE_LEFT, vectorL);
		}
		if (currentDirection == Direction.up) {
			int yNew = y+1;
			if (yNew > 89)
				Notify (GAME_OVER);
			if (Board [x, yNew] == 'x') {
				Debug.Log ("CrashU");
				Notify (GAME_OVER);
			}
			if (Board [x, yNew] == 'o') {
				Debug.Log ("APPLE");
				SnakeLength++;
				Vector3 applePos = new Vector3 (x, yNew, 1);

				Notify (EAT_APPLE, applePos);
//				Board [x, yNew] = 'x';
//				Board [x, y] = 'x';
			}

			if (Board [x, yNew] == 's') {
				Debug.Log ("APPLE_EXTRA");
				SnakeLength++;
				Vector3 applePos = new Vector3 (x, yNew, 1);

				Notify (EAT_APPLE_EXTRA, applePos);
				//				Board [x, yNew] = 'x';
				//				Board [x, y] = 'x';
			}

			Vector3 vectorU = new Vector3 (x, yNew, 1);
			currentHeadPos = vectorU;
			Board [x, yNew] = 'x';
			Notify (MOVE_UP, vectorU);

		}
		if (currentDirection == Direction.down) {
			int yNew = y - 1;

			if (yNew < 0)
				Notify (GAME_OVER);

			if (Board [x, yNew] == 'x') {
				Debug.Log ("CrashD");
				Notify (GAME_OVER);
			}
	
			if (Board [x, yNew] == 'o'){
				Debug.Log ("APPLE");
				SnakeLength++;
				Vector3 applePos = new Vector3 (x, yNew, 1);
				Notify (EAT_APPLE, applePos);
//				Board [x, yNew] = 'x';
//				Board [x, y] = 'x';

			}


			if (Board [x, yNew] == 's'){
				Debug.Log ("APPLE_EXTRA");
				SnakeLength++;
				Vector3 applePos = new Vector3 (x, yNew, 1);
				Notify (EAT_APPLE_EXTRA, applePos);
				//				Board [x, yNew] = 'x';
				//				Board [x, y] = 'x';

			}

			Vector3 vectorD = new Vector3 (x, yNew, 1);
			currentHeadPos = vectorD;
			Board [x, yNew] = 'x';
			Notify (MOVE_DOWN, vectorD);
		}
	}

	public void DeleteFromBoard(Vector3 vector3){
		int x = (int)vector3.x;
		int y = (int)vector3.y;
		Board [x, y] = '-';
	}

	public void ResetDirection(){
		currentDirection = Direction.up;
	}

	public void ChangeDirectionL(){
		switch (currentDirection) {
		case Direction.up:
			currentDirection = Direction.left;
			break;
		case Direction.left:
			currentDirection = Direction.down;
			break;
		case Direction.down:
			currentDirection = Direction.right;
			break;
		case Direction.right:
			currentDirection = Direction.up;
			break;
		}
	}

	public void ChangeDirectionR(){
		switch (currentDirection) {
		case Direction.up:
			currentDirection = Direction.right;
			break;
		case Direction.left:
			currentDirection = Direction.up;
			break;
		case Direction.down:
			currentDirection = Direction.left;
			break;
		case Direction.right:
			currentDirection = Direction.down;
			break;
		}
	}


	public void CheckPlatform(){
		if (Application.isMobilePlatform)
			Notify (MOBILE_PLATFORM);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (currentGameState == Turn.inGame) {
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				ChangeDirectionL ();
				Debug.Log ("TurnLeft");
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				Notify (BACK_TO_MAIN_MENU);
			}
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				ChangeDirectionR ();
				Debug.Log ("TurnLeft");
			} else {
				timeLeft -= Time.deltaTime;
				if (timeLeft < 0) {
					if ( app.view.GameView.objects <= 4)
					timeLeft = 0.2f;
					if (app.view.GameView.objects > 4)
						timeLeft = 0.1f;
					Vector3 vector = GetCurrentVectorPos ();
					Notify (TIME_TO_MOVE, vector);

				}
			}
		}
	}
}

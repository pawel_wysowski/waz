﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class GameController : Controller<SnakeApplication> {



	public override void OnNotification (string p_event, Object p_target, params object[] p_data)
	{
		switch (p_event) {
		case GameModel.GENERATED_FOOD:
			app.view.GameView.UpdateFoodPosition ((Vector3)p_data[0]);
			break;

		case GameModel.GENERATED_EXTRA_FOOD:
			app.view.GameView.UpdateExtraFoodPosition ((Vector3)p_data[0]);
			break;

		case GameModel.TIME_TO_MOVE:
			app.model.GameModel.MoveSnakeBody ((Vector3)p_data[0]);
			break;

		case GameModel.MOVE_UP:
			app.view.GameView.UpdateBodyViewPos((Vector3)p_data[0]);
				break;

		case GameModel.MOVE_RIGHT:
			app.view.GameView.UpdateBodyViewPos((Vector3)p_data[0]);
			break;

		case GameModel.MOVE_LEFT:
			app.view.GameView.UpdateBodyViewPos((Vector3)p_data[0]);
			break;

		case GameModel.MOVE_DOWN:
			app.view.GameView.UpdateBodyViewPos((Vector3)p_data[0]);
			break;

		case GameModel.MOBILE_PLATFORM:
			app.view.GameView.AddAlternativeButton ();
			break;

		case MainMenuView.START_BUTTON_CLICKED:
			app.model.GameModel.CheckPlatform ();
			app.model.GameModel.ResetBoard ();
			app.view.GameView.ResetObjects ();
			app.model.GameModel.ResetDirection ();
			app.view.GameView.PlayerPointsView.gameObject.SetActive (true);
			app.view.GameView.PlayerPointsView.Init ();
			app.model.GameModel.StartBodyPosition ();
			app.view.GameView.BoardView.gameObject.SetActive (true);
			app.view.GameView.MainMenuView.gameObject.SetActive (false);

			app.model.GameModel.ChangeGameState ();

			break;
		case Button1View.TURN_RIGHT:
			app.model.GameModel.ChangeDirectionR ();
			break;

		case Button2View.TURN_LEFT:
			app.model.GameModel.ChangeDirectionL ();
			break;

		case GameModel.GAME_OVER:
			app.model.GameModel.ChangeGameState ();
			app.model.GameModel.ResetTime ();
			app.view.GameView.DestroySnake ();
			app.view.GameView.DestroyApple ();
			app.view.GameView.DestroyAppleExtra ();
			app.view.GameView.BoardView.gameObject.SetActive (false);
			app.view.GameView.EndGameView.gameObject.SetActive (true);
			app.view.GameView.PlayerPointsView.gameObject.SetActive (false);
			app.view.GameView.EndGameView.UpdateLivesView ();
			app.view.GameView.EndGameView.Init ();
			app.view.GameView.DestroyButtons ();
			break;

		case GameModel.BACK_TO_MAIN_MENU:
			app.model.GameModel.ChangeGameState ();
			app.model.GameModel.ResetTime ();
			app.view.GameView.DestroySnake ();
			app.view.GameView.DestroyApple ();
			app.view.GameView.DestroyAppleExtra ();
			app.view.GameView.BoardView.gameObject.SetActive (false);
			app.view.GameView.PlayerPointsView.gameObject.SetActive (false);
			app.view.GameView.MainMenuView.gameObject.SetActive (true);
			app.view.GameView.MainMenuView.Init ();
			break;

		case MainMenuView.EXIT_BUTTON_CLICKED:
			Application.Quit ();
			break;

		case GameModel.EAT_APPLE_EXTRA:
			app.view.GameView.IncreaseExtra ((Vector3)p_data [0]);
			app.view.GameView.DestroyAppleExtra ();
			app.view.GameView.DestroyApple ();
			app.view.GameView.PlayerPointsView.UpdateLivesView ();
			app.model.GameModel.RandomFoodPosition ();
			break;
		case GameModel.EAT_APPLE:
			app.view.GameView.Increase ((Vector3)p_data [0]);
			//app.view.GameView.IncreaseExtra((Vector3)p_data[0]);
			app.view.GameView.DestroyApple ();
			app.view.GameView.DestroyAppleExtra ();
			app.view.GameView.PlayerPointsView.UpdateLivesView ();
			app.model.GameModel.RandomExtraFoodPosition ();

			break;

		case GameView.LAST_PART_POS:
			app.model.GameModel.DeleteFromBoard ((Vector3)p_data [0]);
			break;


		case GameModel.START_POS:
			app.view.GameView.StartingBody ((Vector3)p_data [0]);
			break;

		case EndGameView.BACK_BUTTON_CLICKED:
			app.view.GameView.EndGameView.gameObject.SetActive (false);
			app.view.GameView.MainMenuView.gameObject.SetActive (true);
			app.view.GameView.MainMenuView.Init ();
			break;
		}
	}

	// Use this for initialization
	void Start () {
		app.view.GameView.MainMenuView.gameObject.SetActive (true);
		app.view.GameView.MainMenuView.Init ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class Button2View : View<SnakeApplication> {

	public const string TURN_LEFT = "Button2View.OnClick";

	public void OnMouseDown(){
		Debug.Log ("CLICK");
		Notify (TURN_LEFT);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

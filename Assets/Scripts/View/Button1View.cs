﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class Button1View : View<SnakeApplication> {

	public const string TURN_RIGHT = "Button1View.OnClick";

	public void OnMouseDown(){
		Debug.Log ("CLICK");
		Notify (TURN_RIGHT);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class GameView :  View<SnakeApplication>{

	public const string LAST_PART_POS = "GameView.LastPartPos";

	[SerializeField] MainMenuView mainMenu;
	public MainMenuView MainMenuView { get { return mainMenu; } }

	[SerializeField] BoardView boardView;
	public BoardView BoardView { get { return boardView; } }

	[SerializeField] PlayerPointsView playerPointsView;
	public PlayerPointsView PlayerPointsView { get { return playerPointsView; } }

	[SerializeField] EndGameView endGameView;
	public EndGameView EndGameView { get { return endGameView; } }

	[SerializeField] public GameObject snakeBodyPrefab;
	[SerializeField] public GameObject extraFoodPrefab;
	[SerializeField] public GameObject button1Prefab;
	[SerializeField] public GameObject button2Prefab;

	public List<GameObject> bodyList;
	public List<GameObject> appleList;
	public List<GameObject> appleExtraList;
	public List<GameObject> buttonList;
	public int objects = 1;
	public Vector3 lastPos;

	public void StartingBody(Vector3 vector3){
		GameObject bodyPart = Instantiate (snakeBodyPrefab, vector3, Quaternion.identity);
		bodyList.Add (bodyPart);
	}

	public void UpdateBodyViewPos(Vector3 vector3){
		int lastPart = bodyList.Count - objects;
		lastPos = bodyList [lastPart].transform.position;
		Notify (LAST_PART_POS, lastPos);
		Destroy (bodyList [lastPart]);

		GameObject body = Instantiate (snakeBodyPrefab, vector3, Quaternion.identity);
		bodyList.Add (body);

		//bodyList.Remove (bodyList [lastPart]);
	}

	public void AddAlternativeButton(){
		Vector3 vector1 = new Vector3 (95, 20, 1);
		Vector3 vector2 = new Vector3 (-30, 20, 1);
		GameObject button = Instantiate (button1Prefab, vector1, Quaternion.identity);
		buttonList.Add (button);
		GameObject button2 = Instantiate (button2Prefab, vector2, Quaternion.identity);
		buttonList.Add (button2);
	}

	public void ResetObjects(){
		objects = 1;
	}

	public void Increase(Vector3 vector3){
		objects++;
//		bodyList.Add(appleList[appleList.Count-1]);
//		int lastPart = bodyList.Count - (objects-1);
//		Vector3 vector3 = lastPos;
			//bodyList[lastPart].gameObject.transform.position;
		GameObject body = Instantiate (snakeBodyPrefab, vector3, Quaternion.identity);
		bodyList.Add (body);
	}

	public void IncreaseExtra(Vector3 vector3){
		objects = objects + 3;
		for(int i = 0; i< 3; i++){
			
			GameObject body = Instantiate (snakeBodyPrefab, vector3, Quaternion.identity);
			bodyList.Add(body);
			vector3.x++;
		}
	}

	public void DestroyApple(){
		Destroy(appleList[appleList.Count-1]);
		//appleList.Clear ();
	}

	public void DestroyAppleExtra(){
		Destroy(appleExtraList[appleExtraList.Count-1]);
		//appleExtraList.Clear ();
	}
		
	public void DestroyButtons(){
		foreach (GameObject go in buttonList)
			Destroy (go);
		buttonList.Clear ();
	}

	public void UpdateFoodPosition(Vector3 vector3){
		GameObject foodPart = Instantiate (snakeBodyPrefab, vector3, Quaternion.identity);
		appleList.Add (foodPart);
	}

	public void UpdateExtraFoodPosition(Vector3 vector3){
		GameObject extraFoodPart = Instantiate (extraFoodPrefab, vector3, Quaternion.identity);
		appleExtraList.Add (extraFoodPart);
	}

	public void DestroySnake(){
		foreach (GameObject go in bodyList)
			Destroy (go);
		bodyList.Clear ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

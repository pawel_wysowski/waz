﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class PlayerPointsView : View<SnakeApplication> {



	[SerializeField] Text snakeLengthText;

	public void Init(){
		UpdateLivesView ();
		this.gameObject.SetActive (true);
	}
	public void UpdateLivesView(){
		snakeLengthText.text = string.Format ("Points: {0}", app.view.GameView.objects -1 );
	}



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

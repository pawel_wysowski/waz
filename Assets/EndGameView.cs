﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using thelab.mvc;

public class EndGameView : View<SnakeApplication> {


	public const string BACK_BUTTON_CLICKED = "EndGameView.BackButtonClicked";

	[SerializeField] Text snakeLengthText;
	[SerializeField] Button backToMainMenuButton;


	public void Init(){
		UpdateLivesView ();
		SetBackButton ();
		this.gameObject.SetActive (true);
	}

	public void SetBackButton(){
		backToMainMenuButton.onClick.RemoveAllListeners();
		backToMainMenuButton.onClick.AddListener (() => {
			Debug.Log("MainMenuView Exit Clicked");
			Notify(BACK_BUTTON_CLICKED);
		});
	}

	public void UpdateLivesView(){
		snakeLengthText.text = string.Format ("Points: {0}", app.view.GameView.objects -1 );
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

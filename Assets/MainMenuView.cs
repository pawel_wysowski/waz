﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;
using UnityEngine.UI;

public class MainMenuView : View<SnakeApplication> {


	[SerializeField] Button startButton;
	[SerializeField] Button exitButton;

	public const string EXIT_BUTTON_CLICKED = "MainMenuView.ExitButtonClicked";
	public const string START_BUTTON_CLICKED = "MainMenuView.StartButtonClicked";

	public void Init(){
		SetStartButton ();
		SetExitButton ();

	}

	void SetStartButton (){
		startButton.onClick.RemoveAllListeners();
		startButton.onClick.AddListener (() => {
			Debug.Log("MainMenuView Start Clicked");
			Notify(START_BUTTON_CLICKED);
		});
	}

	void SetExitButton (){
		exitButton.onClick.RemoveAllListeners();
		exitButton.onClick.AddListener (() => {
			Debug.Log("MainMenuView Exit Clicked");
			Notify(EXIT_BUTTON_CLICKED);
		});
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using thelab.mvc;

public class BoardView : View<SnakeApplication> {


	[SerializeField] SpriteRenderer boardSpriteRenderer;

	// Use this for initialization
	void Start () {
		SetBoardSpriteRenderer ();
	}
	
	// Update is called once per frame
	void Update () {
		SetBoardSpriteRenderer ();
	}


	void SetBoardSpriteRenderer(){
		boardSpriteRenderer.sprite = this.boardSpriteRenderer.sprite;
	}

}
